$(document).ready(function () {

  $('.mybutton').on('click',function() {
    var element = $(this).attr('id');
    $('.subart').each(function() {
      var subart = $(this).attr('id');
      if(element == subart){
        $("div[id*= "+subart+"]").css("display","block");
        $(".subcat").css("display","none");
        $(".listItem").text(subart);
        window.location.hash = "subcategory="+subart;
      }
    }); 
  });

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
    return null;
  }

  var price = readCookie('price');

  $('.currency').change(function () {
    var currency = $(this).val();
    $.post('/currency/send?currency='+currency+'&price='+price).
    done(function(data){
      $('.price').html(data);
    });
  });
    
  var myCookie = readCookie('newCurrency');
  if(myCookie!=''||myCookie!=null){
    if($(".currency option[value="+myCookie+"]").length > 0){
      $(".currency option[value="+myCookie+"]").attr('selected','selected');
    }
  }

});//end document