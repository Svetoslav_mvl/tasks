/**
 * ProductsController
 *
 * @description :: Server-side logic for managing Products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  show: function(req,res,next) {
    var param = req.query;
    Products.find({'primary_category_id':param.primary_category_id}).exec(function findProducts(err,products) {
      if(err) return next(err);
      if(!products) return next();
      var params = param.sub_name;
      return res.view('pages/products',{
        products: products,
        params:params
      });
    });
  },

  info: function(req,res,next) {
    var param = req.query;
    var findPrice = req.cookies.price;
    var findNewCurrencyValue = req.cookies.newCurrencyValue;
    Products.find({'name':param.name}).exec(function findProduct(err,product) {
      if(err) return next(err);
      if(!product) return next();
      var params = param.sub_name;
      CurrencyType.find().exec(function findCurrency(err,currencies){
        if(err) return next(err);
        if(!currencies) return next();
        /*     */
        var price='';
        for(var i=0;i<product.length;i++){
          price += product[i].price.toString();
        }
        res.cookie('price',price,{expires: new Date(Date.now() + 100000)});
        
          return res.view('pages/productInfo',{
            product: product,
            params: params,
            currencies: currencies,
            newPrice: (findPrice === undefined) ? price: findNewCurrencyValue
          });

      });//end CurrencyType.find 
    });//end Product.find
  }//end method
};//end export