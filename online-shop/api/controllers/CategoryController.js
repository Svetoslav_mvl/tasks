/**
 * CategoryController
 *
 * @description :: Server-side logic for managing Categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  index: function(req,res,next) {
    var param = req.allParams();
    Category.find(param).exec(function findCategory(err,categories) {
      if(err) return next(err);
      if(!categories) return next();
      return res.view('pages/homepage',{
        categories: categories
      });
    });
  }
  
};

