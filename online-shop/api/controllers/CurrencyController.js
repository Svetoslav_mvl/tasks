/**
 * CurrencyController
 *
 * @description :: Server-side logic for managing Currencies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  send: function(req,res,next) {

    var soap     = require('soap');
    var params   = req.query;
    var today    = new Date();
    var date     = today.getDate()-1;
    var month    = today.getMonth()+1;
    var year     = today.getFullYear();
    var hours    = today.getHours();
    var minutes  = today.getMinutes();
    var price    = params.price;
    var currency = params.currency;
    var usd      = 'USD';
    
    month    = (month < 10) ? '0' + month : month;
    minutes  = (minutes < 10) ? '0' + minutes : minutes;
    var url  = 'http://infovalutar.ro/curs.asmx?wsdl';
    var args = {dt: year+'-'+'05'+'-'+'30'+'-'+hours+':'+minutes};
    //month+date

    soap.createClient(url, function(err,client){
      client.getall(args, function(err,result){
        var arr   = [];
        var currs = result.getallResult.diffgram.DocumentElement.Currency;
        
        
        for(var i=0;i<currs.length;i++){
          arr[currs[i].IDMoneda]=currs[i].Value;
        } 

        var find = function(arr,val) {
          for (var i in arr){
            if (i == val) {                   
              return arr[i];
            }  
          }
          return false;
        }

        function calculate(price,usd,currency){
          return price*usd/currency;
        }

        var result = Math.round(calculate(price,find(arr,usd),find(arr,currency)));
        var data   = result.toString();

        res.cookie('newCurrency',currency,{expires: new Date(Date.now() + 100000)});
        res.cookie('newCurrencyValue',data,{expires: new Date(Date.now() + 100000)});
        res.send(data);
        res.end();
      });

    });
  }
};