/**
 * ProductsController
 *
 * @description :: Server-side logic for managing products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'showProducts': function(req,res) {
    res.view();
  },
  'product-info':function (req,res) {
    res.view();
  }
};

