'use strict';
module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin:{
      combine:{
        files:{
          'css/style.min.css':[ 'css/style.css']
        }
      }
    },
    sass:{
      dist:{
        files:{
          'css/styles.css':'sass/style.scss'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.registerTask("default",['sass','cssmin']);
};