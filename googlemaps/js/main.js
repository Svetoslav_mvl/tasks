var geocoder,google,marker,latlng,map;

function createMap(){
    var mapOptions = {
        zoom: 15,
        center: latlng
    };
    latlng = new google.maps.LatLng(42.697698, 23.321829);
    map = new google.maps.Map($('#googleMap')[0], mapOptions);
};

function codeAddress(address) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        createMap();               
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        map = new google.maps.Map($("#googleMap")[0],marker);
        }
    });
};

function initialize2(){
    geocoder = new google.maps.Geocoder();
    latlng = new google.maps.LatLng(42.697698, 23.321829);
    var mapProp = {
        center:latlng,
        zoom:12,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    }; 
    map = new google.maps.Map($("#googleMap")[0],mapProp);
    google.maps.event.addListener(map,'click', function(event) {
        placeMarker(event.latLng);
    }); 
    google.maps.event.addDomListener(window,'load', initialize2);
};

function placeMarker(location) {
    if (marker) {
        //if marker already was created change positon
        marker.setPosition(location);
        geocoder.geocode({'location': location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $("#address").attr("value", results[0].formatted_address);
            } 
        });
    } 
    else {
        //create a marker
        marker = new google.maps.Marker({
            position: location,
            map: map
        });
        geocoder.geocode({'location': location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $("#address").attr("value", results[0].formatted_address);
            } 
        });
    }
};  

function initMap(){
    var input,autocomplete;
    input = /** @type {!HTMLInputElement} */($('#address')[0]);
    autocomplete = new google.maps.places.Autocomplete(input);
    createMap();   
 };
 
$(document).ready(function(){
    $("#find").on("click", initialize2);
    // Add Gustom Rule in library from each field and Validate data with REGEX 
    //validator add method with regex for input-name
    $.validator.addMethod("nameRegex", function(value, element) {
        return this.optional(element) || /[a-zA-Z\u0410-\u044f]/i.test(value);
    });
    //validator add method with regex for input-address
    $.validator.addMethod("addressRegex", function(value, element) {
        return this.optional(element) || /[",:.\d a-zA-Z\u0410-\u044f]/i.test(value);
    });
     //validator add method with regex for input-phone
    $.validator.addMethod("phoneRegex", function(value, element) {
        return this.optional(element) || /^[0-9]*$/i.test(value);
    });
    //validator add method with regex for input-phone
    $.validator.addMethod("webRegex", function(value, element) {
        return this.optional(element) || /^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$/i.test(value);
    });
    //validator add method with regex for input-email
    $.validator.addMethod("emailRegex", function(value, element) {
        return this.optional(element) || /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/i.test(value);
    });
    $(function(){
        $('#user-form').validate({
            rules:{
                name:{
                required:true,
                minlength: 5,
                maxlength: 32,
                nameRegex: true
                },
                address:{
                required: true,
                addressRegex: true
                },
                email:{
                    required: true,
                    email: true,
                    maxlength: 32, 
                    emailRegex: true
                } ,
                phone:{
                    required:true,
                    minlength: 6,
                    maxlength: 13, 
                    phoneRegex: true
                },
                web:{
                    required: true,
                    webRegex: true
                }
            },
            messages:{
                name:{                    
                    required: "Please enter your name!",
                    minlength: "Please enter at least 5 characters!",
                    maxlength: "Please enter less than 32 characters!",
                    nameRegex: "Please enter only letters!"
                },
                address:{                    
                    required: "Please enter your address!",
                    addressRegex: "The address field must contain only letters, numbers, or dashes!"
                },
                email:{                    
                    required: "Please enter your email!",
                    email: "Please enter a valid email!",
                    maxlength: "Please enter less than 50 characters!",
                    emailRegex: "The email field is not in the correct format!"
                },
                phone:{                    
                    required: "Please enter your phone number!",
                    minlength: "Please enter at least 6 numbers!",
                    maxlength: "Please enter less than 13 characters!",
                    phoneRegex: "Please enter only numbers!"
                },
                web:{                    
                    required: "Please enter your websuite!",
                    webRegex: "The url field is not in the correct format!"
                }
            }
        });//end validate
    });// end function   
    function resize(){
        $("#googleMap").width(600);
        $("#googleMap").height(400);
        google.maps.event.trigger(map, 'resize');
    };
    $('#submit').click(function() {
        if ($('#user-form').valid()) {
            var address,name,email,phone,web,obj;
            address = $("#address").val();
            name = $("#name").val();
            email = $("#email").val();
            phone = $("#phone").val();
            web = $("#web").val();       
            obj = {
                Name:name,
                Address:address,
                Email:email,
                Phone:phone,
                Web:web
            };
            localStorage.setItem("obj", JSON.stringify(obj));
            resize();
            codeAddress(address);
            $('#user-form').submit(false);
            $("#user-form")[0].reset(); 
            $('#address').val("");
            $('#address').attr("disable",true);
        }
    });  
    $('#find').click(function(){
        resize();
    });
}); //end document